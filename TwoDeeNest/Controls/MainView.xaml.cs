﻿using SolidWorks.Extended;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwoDeeNest.Controls;
using TwoDeeNest.TwoDeeNest_e;

namespace TwoDeeNest.Controls
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        #region variables declaration
        BindingList<POCOS.Component> Components = new BindingList<POCOS.Component>();
        SldWorks swApp;
        ModelDoc2 swModel;
        MathUtility swMathUtility;

        #endregion

        public MainView(SldWorks _swApp)
        {
            swApp = _swApp;
            Settings.Init();

            InitializeComponent();

            // attach event handlers to canvas
            ComponentPreview.SizeChanged += ComponentPreview_SizeChanged;
        }
        // redraw component if canvas has bee changed
        private void ComponentPreview_SizeChanged(object sender, SizeChangedEventArgs e)
        {
      
            ComponentsGrid_SelectionChanged(ComponentPreview, null);
        }

        CancellationTokenSource swCancellationTokenSource;
        private void GrabButton_Click(object sender, RoutedEventArgs e)
        {
            swCancellationTokenSource = new CancellationTokenSource();
            GrabPartsFromAssembly(swCancellationTokenSource.Token);
        }


        private async void GrabPartsFromAssembly(CancellationToken token)
        {
            swModel = swApp.ActiveDoc as ModelDoc2;
            if (swModel == null)
            {
                MessageBox.Show("Cannot find model");
                return;
            }
            swMathUtility = swApp.GetMathUtility() as MathUtility;
            if (swModel.GetType() == (int)swDocumentTypes_e.swDocASSEMBLY)
            {
                Components.AllowEdit = true;
                Components.AllowNew = true;
                Components.AllowRemove = true;
                var ComponentsList = new List<POCOS.Component>();
                AssemblyDoc swAssembly = swModel as AssemblyDoc;
                Component2[] swComponents = swAssembly.GetComponents(false, true, true);
                int count = swComponents.Length;
                int iterator = 0;
                ModelView modelView = swModel.ActiveView as ModelView;
                modelView.EnableGraphicsUpdate = false;
                foreach (var Component in swComponents)
                {
                    if (token.IsCancellationRequested)
                    {
                        Status.Text = "Operation cancelled";
                        return;
                    }


                    iterator++;
                    string counter = string.Format(@"({0}/{1}) ", iterator, count);
                    if (Components.Count > 0)
                        ComponentsList = Components.ToList<POCOS.Component>();
                    if (ComponentsList.Exists((x) => x.FileName == Component.GetPathName()))
                    {

                        POCOS.Component component = ComponentsList.Find((x) => x.FileName == Component.GetPathName());
                        component.Count++;
                    }
                    else
                    {
                        ModelDoc2 ModelDoc = default(ModelDoc2);
                        try
                        {


                            Status.Text = counter + "Processing " + Component.GetPathName() + " ...";

                            ModelDoc = await swApp.OpenPartAsync(Component.GetPathName());

                            if (ModelDoc == null)
                            {
                                Status.Text = counter + "Unable to open " + Component.GetPathName() + " ...";
                                continue;
                            }

                            Status.Text = counter + "Getting geometry of " + Component.GetPathName() + " ...";
                            var Feature = await Helper.GetFeatureFromSheetMetal(ModelDoc, swApp);
                            if (Feature == null)
                            {
                                Status.Text = counter + "Skipping " + Component.GetPathName() + " ...";
                                await swApp.CloseDocAsync(ModelDoc);
                                continue;
                            }


                            Status.Text = counter + "Processing geometry of " + Component.GetPathName() + " ...";
                            var Face = await Helper.GetLargestFace2(Feature);
                            if (Face == null)
                            {
                                Status.Text = counter + "Skipping " + Component.GetPathName() + " ...";
                                await swApp.CloseDocAsync(ModelDoc);
                                continue;
                            }

                            Status.Text = counter + "Recreating geometry of " + Component.GetPathName() + " ...";
                            var Shapes = await Helper.GetShapesFromFace(ModelDoc, Face, swMathUtility);
                            Status.Text = counter + "Getting material information from " + Component.GetPathName() + " ...";
                            var Material = await Helper.GetMaterial(ModelDoc);
                            Status.Text = counter + "Getting thickness of " + Component.GetPathName() + " ...";
                            var Thickness = await Helper.GetThickness(ModelDoc);
                            Status.Text = counter + "Creating TwoDeeNest component...";
                            var NewComponent = new POCOS.Component(Shapes, Component.GetPathName(), Material, Thickness);

                            await swApp.CloseDocAsync(ModelDoc, Feature);
                            NewComponent.Count++;
                            Components.Add(NewComponent);
                            ComponentsGrid.ItemsSource = Components;
                            ComponentsGrid.Items.Refresh();

                        }
                        catch (Exception ee)
                        {
                            // treat logs
                            Status.Text = counter + "An error occured. Please see logs.";
                            Debug.Print(ee.Message);
                            await swApp.CloseDocAsync(ModelDoc);
                            continue;
                        }
                    }
                }
                modelView.EnableGraphicsUpdate = true;
                Status.Text = "Ready!";


            }


        }

        private void ComponentsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // draw component
            DataGrid grid = sender as DataGrid;
            var Component = grid.SelectedItem as POCOS.Component;
            if (Component != null)
            {
                var shapes = Component.GetShapes();
                ComponentPreview.DrawShapes(shapes, Component);
                ComponentPreview.ZoomToFit(Component);

            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            if (swCancellationTokenSource != null)
                swCancellationTokenSource.Cancel();
        }



    }
      
   
}

