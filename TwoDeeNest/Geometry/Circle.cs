﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using TwoDeeNest.TwoDeeNest_e;

namespace TwoDeeNest.Geometry
{
    enum  Direction 
    {
        Unknown,
        Cloackwise,
        CounterCloackwise      
    }

    class Circle  : IShape
    {
        public Point StartPoint { get; private set; }
        public Point EndPoint { get; private set; }
        public Point Center { get; private set;  }
        public Direction CircleDirection { get; set; }
        public Point MidPoint { get; set; }
        public double Radius { get; private set; }
        public bool IsTranslated { get; set; }
    
        private double Length;
        /// <summary>
        /// Use this constructor to define an arc 
        /// </summary>
        /// <param name="startPoint"></param>
        /// <param name="endPoint"></param>
        /// <param name="center"></param>
        /// <param name="direction"></param>
        /// <param name="radius"></param>
        /// <param name="color"></param>
        public Circle(Point startPoint, Point endPoint, Point center, Direction direction, double radius, double length)
        {
       
            Length = length;
            StartPoint = startPoint;
            EndPoint = endPoint;
            Center = center;
            CircleDirection = direction;
            Radius = radius;
            this.Scale(1000);
        }

        public Circle(Point center, double radius)
        {
            StartPoint = null;
            EndPoint = null;
            Center = center; 
            Radius = radius;
            this.Scale(1000);
        }
        public Circle()
        {

            
        }

        public double getLength()
        {
            if (IsComplete())
                return Math.PI * 2 * Radius; 
            return Length;
        }

        public bool IsComplete()
        {
            return StartPoint == null ? true : false; 
        }

        ShapeType_e IShape.GetType()
        {
            return ShapeType_e.Circle;
        }

        public void Translate(double x, double y, double z)
        {
           

            if (StartPoint != null)
            {
                StartPoint.X += x;
                StartPoint.Y += y;
                StartPoint.Z += z;
            }
            if (EndPoint != null)
            {
               
                EndPoint.X += x;
                EndPoint.Y += y;
                EndPoint.Z += z;
            }

            if (Center != null)
            {
                Center.X += x;
                Center.Y += y;
                Center.Z += z;

            }
        }
        public void Scale(int factor)
        {
            if (Center != null)
            {
                Center.X *= factor;
                Center.Y *= factor;
                Center.Z *= factor;
            }
            
            if (StartPoint != null)
            {
                StartPoint.X *= factor;
                StartPoint.Y *= factor;
                StartPoint.Z *= factor;

             
            }
            if (EndPoint != null)
            {
                EndPoint.X *= factor;
                EndPoint.Y *= factor;
                EndPoint.Z *= factor;
            }
            
            Radius *= factor;
            Length *= factor;
        }


        public Point[] Box()
        {
           
            
            return new Point[] { new Geometry.Point(Center.X - Radius, Center.Y - Radius, Center.Z - Radius), new Geometry.Point(Center.X + Radius, Center.Y + Radius, Center.Z + Radius) };
             
            
        }

        
    }
}
