﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoDeeNest.TwoDeeNest_e;

namespace TwoDeeNest.Geometry
{
    class Ellipse : IShape
    { 
        public Point Center { get; private set; }
        public Vector MajorAxis { get; set; }
        public Vector MinorAxis { get; set; }
        public double LargeAxis { get; set; }
        public double smallAxis { get; set; }
        public Direction CircleDirection { get; set; } 
        public Ellipse(Point center, Direction direction)
        {
           
            Center = center;
            CircleDirection = direction;
    
        }
        /// <summary>
        /// Get the type of this shape 
        /// </summary>
        /// <returns></returns>
        public new ShapeType_e GetType()
        {
            return ShapeType_e.Ellipse;
        }
        public Point[] Box()
        {
            return null;
        }

        public void Translate(double x, double y, double z)
        {
             
            throw new NotImplementedException();
        }
        public void Scale(int factor)
        {
            throw new NotImplementedException();
        }
    }
}
