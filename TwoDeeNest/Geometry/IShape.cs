﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoDeeNest.TwoDeeNest_e;

namespace TwoDeeNest.Geometry
{
    public interface IShape
    {
        ShapeType_e GetType();
        Point[] Box();
        void Translate(double x, double y, double z);
       
    }

    
}
