﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoDeeNest.TwoDeeNest_e;

namespace TwoDeeNest.Geometry
{
    public class Line : IShape
    {
        public Point StartPoint { get; set; }
        public Point EndPoint { get; set; }
        public bool IsTranslated { get; set; }

        public Line(Point startPoint, Point endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
            this.Scale(1000);
        }
        public void Translate(double x, double y, double z)
        {
            StartPoint.X += x;
            StartPoint.Y += y;
            StartPoint.Z += z;
            EndPoint.X += x;
            EndPoint.Y += y;
            EndPoint.Z += z;


        }
        public void Scale(int factor)
        {
            StartPoint.X *= factor;
            StartPoint.Y *= factor;
            StartPoint.Z *= factor;

            EndPoint.X *= factor;
            EndPoint.Y *= factor;
            EndPoint.Z *= factor;

        }
        public new ShapeType_e GetType()
        {
            return ShapeType_e.Line;
        }
        public Point[] Box()
        {
            return new Point[] { StartPoint, EndPoint };
        }

    }
}
