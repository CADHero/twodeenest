﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDeeNest.Geometry
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Point()
        {

        }

        public Point(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
            
        }
        public Point(double[] arr)
        {
            X = arr[0];
            Y = arr[1];
            Z = arr[2];

        }

        public void ZeroAxis(TwoDeeNest_e.CoordinateToZero_e Perf)
        {
            switch (Perf)
            {
                case TwoDeeNest_e.CoordinateToZero_e.Unknown:
                    break;
                case TwoDeeNest_e.CoordinateToZero_e.X:
                    {
                        X = Z;
                        Z = 0;

                    }
                    break;
                case TwoDeeNest_e.CoordinateToZero_e.Y:
                    {
                        Y = Z;
                        Z = 0;

                    }
                    break;
                case TwoDeeNest_e.CoordinateToZero_e.Z:
                    {
                        Z = 0;
                    }
                    break;
                default:
                    break;
            }

        }

       

    }


}
