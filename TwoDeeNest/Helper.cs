﻿using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using WPFShapes = System.Windows.Shapes;
using TwoDeeNest.Geometry;
using System.Windows.Media;
using Win = System.Windows;
using TwoDeeNest.TwoDeeNest_e;
using SolidWorks.Interop.swcommands;

namespace TwoDeeNest
{
    public static class Helper
    {


        public static Task<double> GetThickness(ModelDoc2 swModel)
        {

            return Task<double>.Run(() =>
            {

                Feature swFeature = swModel.FirstFeature() as Feature;
                if (swFeature == null)
                    return 0;

               
               
                while(swFeature != null)
                {
                    Feature swSubFeature = swFeature.GetFirstSubFeature() as Feature;
                    while (swSubFeature != null)
                    {
                        if (swSubFeature.GetTypeName2() == swTypeNames_str.swTnCutListFolder)
                        {
                            CustomPropertyManager propManager = swSubFeature.CustomPropertyManager;
                            string val, Rval;
                            propManager.Get2("Sheet Metal Thickness", out val, out Rval);
                            if (!string.IsNullOrEmpty(Rval))
                                return Convert.ToDouble(Rval);

                        }

                        swSubFeature = swSubFeature.GetNextSubFeature() as Feature;
                    }
                 swFeature = swFeature.GetNextFeature() as Feature;

                }
                return 0;
            });

        }
        public static Task<string> GetMaterial(ModelDoc2 swModel)
        {

            return Task<string>.Run(() =>
            {

                Feature swFeature = swModel.FirstFeature() as Feature;
                if (swFeature == null)
                    return null;
                Debug.Print(swFeature.Name + " [" + swFeature.GetTypeName2() + "]");
                while (swFeature != null)
                {

                    if (swFeature.GetTypeName2() == swTypeNames_str.swTnSolidBodyFolder)
                    {
                        var swBodyFolder = swFeature.GetSpecificFeature2() as BodyFolder;
                        swBodyFolder.UpdateCutList();
                    }

                    Feature swSubFeature = swFeature.GetFirstSubFeature() as Feature;
                   
                    while (swSubFeature != null)
                    {
                        Debug.Print(swSubFeature.Name + " [" + swSubFeature.GetTypeName2() + "]");
                        if (swSubFeature.GetTypeName2() == swTypeNames_str.swTnCutListFolder)
                        {
                            CustomPropertyManager propManager = swSubFeature.CustomPropertyManager;
                            string val, Rval;
                            propManager.Get2("Material", out val, out Rval);
                            if (!string.IsNullOrEmpty(Rval))
                                return Rval;

                        }

                        swSubFeature = swSubFeature.GetNextSubFeature() as Feature;
                    }
                    swFeature = swFeature.GetNextFeature() as Feature;

                }
                return null;
            });

        }
        public static Task<ModelDoc2> OpenPartAsync(this SldWorks swApp, string PathName)
        {
            return Task<ModelDoc2>.Run(() =>
            {
                int error = 0;
                int warning = 0;
                var ret = swApp.OpenDoc6(PathName, (int)swDocumentTypes_e.swDocPART, (int) swOpenDocOptions_e.swOpenDocOptions_Silent,"", ref error, ref warning) as ModelDoc2;
                return ret;
            });

        
        }
        public static Task CloseDocAsync(this SldWorks swApp, ModelDoc2 Model, Feature swFeature = null)
        {
            return Task.Run(() =>
            {
                if (swFeature != null)
                {
                    swFeature.Select(false);
                    Model.EditSuppress2();
                }
                swApp.QuitDoc(Model.GetTitle());
            });


        }
        public static Task<Face2> GetLargestFace2(Feature swBody)
        {

            return Task<Face2>.Run(()=> 
            {

                if (swBody.GetFaceCount() > 0)
                {
                    Dictionary<Face2, double> facesList = new Dictionary<Face2, double>();
                    object[] Faces = swBody.GetFaces();
                    foreach (object swFaceObj in Faces)
                    {
                        Face2 swFace = (Face2)swFaceObj;
                        facesList.Add(swFace, swFace.GetArea());
                    }
 

                    Face2 largestFace = facesList.OrderByDescending(x => x.Value)
                               .First().Key;

                    Surface swSurface = largestFace.GetSurface() as Surface;
                    
                    if (swSurface != null)
                    {
                        if (swSurface.IsPlane())
                            return largestFace;
                        else
                            return null;
                        
                    }

                    return null;

                }
                else
                    return null;

            });

        }
        public static Task<Feature> GetFeatureFromSheetMetal(ModelDoc2 swModel, SldWorks swApp)
        {
            return Task<Feature>.Run(() => 
            {

                swModel.ClearSelection();

                var Features = swModel.FeatureManager.GetFeatures(false);
                foreach (var Feature in Features)
                {
                    var swFeature = Feature as Feature;
                    if (swFeature.GetTypeName2() == swTypeNames_str.swTnFlatPattern)
                    {


                        swFeature.Select(false);
                        swModel.EditUnsuppress2();
                        return swFeature;
                    }

                }

                return null; 
                   

            });

        }
        public static Task<Point[]> GetPointsFromFace(Face2 swFace, int Refinement, MathUtility swMathUtility)
        {
            return Task<Point[]>.Run(() =>
            {
                List<Point> points = new List<Point>();
                if (swFace.GetEdgeCount() > 0)
                {
                    Loop2 Loop = swFace.GetFirstLoop() as Loop2;
                    while (Loop != null)
                    {
                        object[] Edges = Loop.GetEdges();
                        foreach (var edge in Edges)
                        {
                            Edge swEdge = edge as Edge;
                            CurveParamData swCurveParams = swEdge.GetCurveParams3();
                            double start = swCurveParams.UMinValue;
                            double end = swCurveParams.UMaxValue;
                            double increment = Math.Abs(end - start) / Refinement;
                            double value = start;
                            for (int i = 0; i < Refinement; i++)
                            {
                                double[] ret = swEdge.Evaluate2(value, 2);
                                points.Add(new Point(ret[0], ret[1], ret[2]));
                                value = value + increment;
                            }
                        }


                        Loop = Loop.GetNext();
                    }

                    if (Loop == null)
                    {
                        // get the constant value from the x, y and z
                        double[] faceNormal = swFace.Normal;
                        var faceNormalVector = swMathUtility.CreateVector(faceNormal) as MathVector;
                        var X = swMathUtility.CreateVector(new double[] { 1, 0, 0 }) as MathVector;
                        var Y = swMathUtility.CreateVector(new double[] { 0, 1, 0 }) as MathVector;
                        var Z = swMathUtility.CreateVector(new double[] { 0, 0, 1 }) as MathVector;
                        int retX = 0, retY = 0, retZ = 0;

                        if (areSOLIDWORKSVectorsCollinear(faceNormalVector, X))
                            retX = 1;
                        if (areSOLIDWORKSVectorsCollinear(faceNormalVector, Y))
                            retY = 1;
                        if (areSOLIDWORKSVectorsCollinear(faceNormalVector, Z))
                            retZ = 1;

                        if (retX == 1)
                        {
                            foreach (var point in points)
                            {
                                point.ZeroAxis(TwoDeeNest_e.CoordinateToZero_e.X);
                            }
                        }

                        if (retY == 1)
                        {
                            foreach (var point in points)
                            {
                                point.ZeroAxis(TwoDeeNest_e.CoordinateToZero_e.Y);
                            }
                        }
                        if (retZ == 1)
                        {
                            foreach (var point in points)
                            {
                                point.ZeroAxis(TwoDeeNest_e.CoordinateToZero_e.Z);
                            }
                        }


                        double minX, minY;
                        minX = points.Min(x => x.X);
                        minY = points.Min(x => x.Y);
                        foreach (var point in points)
                        {
                            point.X -= minX;
                            point.Y -= minY;
                        }




                        return points.ToArray();
                    }

                    else
                        return null;
                }
                return null;
            });

        }
        public static Task<Plane_e> GetPlane(Face2 swFace, MathUtility swMathUtility)
        {
            return Task<Plane_e>.Run(() =>
            {
                 
                        // get the constant value from the x, y and z
                        double[] faceNormal = swFace.Normal;
                        var faceNormalVector = swMathUtility.CreateVector(faceNormal) as MathVector;
                        var X = swMathUtility.CreateVector(new double[] { 1, 0, 0 }) as MathVector;
                        var Y = swMathUtility.CreateVector(new double[] { 0, 1, 0 }) as MathVector;
                        var Z = swMathUtility.CreateVector(new double[] { 0, 0, 1 }) as MathVector;
                        int retX = 0, retY = 0, retZ = 0;

                        if (areSOLIDWORKSVectorsCollinear(faceNormalVector, X))
                            retX = 1;
                        if (areSOLIDWORKSVectorsCollinear(faceNormalVector, Y))
                            retY = 1;
                        if (areSOLIDWORKSVectorsCollinear(faceNormalVector, Z))
                            retZ = 1;

                        if (retX == 1)
                        {
                    return Plane_e.YZ;
                        }

                        if (retY == 1)
                        {
                    return Plane_e.ZX;
                     }
                        if (retZ == 1)
                        {
                    return Plane_e.XY;
                   }








                return Plane_e.Unknown; 

                
                
            });

        }

        private static MathTransform CreateNewCoordinateSystem(ModelDoc2 swModel, Face2 swFace)
        {

          

            // create coordinate system
            (swFace as Entity).Select(false);
            swModel.Extension.RunCommand((int)swCommands_e.swCommands_NormalTo, "");
            object[] edges = swFace.GetEdges();
            if (edges != null)
            {
                foreach (var edge in edges)
                {
                    object start = (edge as Edge).GetStartVertex();
                    object end = (edge as Edge).GetEndVertex();

                    if (start != null)
                    {
                        (start as Entity).Select2(true, 1);
                        break;
                    }

                    else if (start == null && end != null)
                    {
                        (end as Entity).Select2(true, 1);

                        break;
                    }

                }
            }
            Feature swFeature = swModel.FeatureManager.InsertCoordinateSystem(false, false, false) as Feature;
            if (swFeature != null)
            {
                CoordinateSystemFeatureData coordinateSystem = swFeature.GetDefinition() as CoordinateSystemFeatureData;
                return coordinateSystem.Transform;

            }

            return null;
        }


        private static Point TransformPointToNewCoordinateSystem(this Point p, MathTransform m, MathUtility swMathUtility)
        {
            if (m == null)
                return p;
            MathPoint point = swMathUtility.CreatePoint(new double[] { p.X, p.Y, p.Z }) as MathPoint;
            MathPoint retPoint = point.MultiplyTransform(m) as MathPoint;
            return new Geometry.Point(retPoint.ArrayData);
        }

        public static Task<IShape[]> GetShapesFromFace(ModelDoc2 swModel, Face2 swFace, MathUtility swMathUtility)
        {
            return Task<IShape[]>.Run(() =>
            {
            List<IShape> Shapes = new List<IShape>();
            if (swFace.GetEdgeCount() > 0)
            {
                Plane_e plane = GetPlane(swFace, swMathUtility).Result;
                // get new coordinate system transformation 
                MathTransform mathT;

                MathVector Normal = swMathUtility.CreateVector(swFace.Normal) as MathVector;
                bool ColX = areSOLIDWORKSVectorsCollinear(Normal,   swMathUtility.CreateVector(new double[] { 1, 0, 0 }) as MathVector);
                bool ColY = areSOLIDWORKSVectorsCollinear(Normal, swMathUtility.CreateVector(new double[] { 0, 1, 0 }) as MathVector);
                bool ColZ = areSOLIDWORKSVectorsCollinear(Normal, swMathUtility.CreateVector(new double[] { 0, 0, 1 }) as MathVector);
                    if (ColX || ColY || ColZ)
                        mathT = Helper.CreateNewCoordinateSystem(swModel, swFace);
                    else
                        mathT = null;
                    Loop2 Loop = swFace.GetFirstLoop() as Loop2;
                    while (Loop != null)
                    {
                        bool loopInner = false;
                        object[] Edges = Loop.GetEdges();
                        if (!Loop.IsOuter())
                        {
                            loopInner = true; 
                        }
                        foreach (var edge in Edges)
                        {
                            Edge swEdge = edge as Edge;
                            var swCurve = swEdge.GetCurve() as Curve;
                            CurveParamData swCurveParams = swEdge.GetCurveParams3();
                            swCurveTypes_e swCurveType = (swCurveTypes_e)Enum.ToObject(typeof(swCurveTypes_e), swCurveParams.CurveType);
                            switch (swCurveType)
                            {
                                case swCurveTypes_e.LINE_TYPE:
                                    {
                                        var startPoint = new Point(swCurveParams.StartPoint);
                                        var endPoint = new Point(swCurveParams.EndPoint);

                                        startPoint = startPoint.TransformPointToNewCoordinateSystem(mathT, swMathUtility);
                                        endPoint = endPoint.TransformPointToNewCoordinateSystem(mathT, swMathUtility);

                                        switch (plane)
                                        {
                                            case Plane_e.Unknown:
                                                break;
                                            case Plane_e.XY:
                                                {
                                                    startPoint.ZeroAxis(CoordinateToZero_e.Z);
                                                    endPoint.ZeroAxis(CoordinateToZero_e.Z);
                                                }
                                                break;
                                            case Plane_e.YZ:
                                                {
                                                    startPoint.ZeroAxis(CoordinateToZero_e.X);
                                                    endPoint.ZeroAxis(CoordinateToZero_e.X);
                                                }
                                                break;
                                            case Plane_e.ZX:
                                                { 
                                                   startPoint.ZeroAxis(CoordinateToZero_e.Y);
                                                    endPoint.ZeroAxis(CoordinateToZero_e.Y);
                                                }
                                                break;
                                            default:
                                                break;
                                        }

                                        var newLine = new Line(startPoint, endPoint);
                                        Shapes.Add(newLine);
                                    }
                                    break;
                                case swCurveTypes_e.CIRCLE_TYPE:
                                    { 
                                        double[] param = swEdge.GetCurveParams();
                                         
                                        // ensure that start parameter is always smaller than end parameter
                                        double[] startendparams = new double[] { -1 * param[6], -1 * param[7] };
                                        double startParam = -1 * startendparams.Max();
                                        double endParam = -1 * startendparams.Min();
                                        double[] circleParms = swCurve.CircleParams;
                                        double radius = circleParms[6];
                                        double start = 0;
                                        double end = 0;
                                        bool isClosed = false;
                                        bool isPeriodic = false;
                                        swCurve.GetEndParams(out start, out end, out isClosed, out isPeriodic);
                                        double[] ret = swCurve.Evaluate2((start+end)*0.5, 2);
                                        double Length = swCurve.GetLength3(startParam, endParam);
                                        var MidPoint = new Point(ret[0], ret[1], ret[2]);
                                        var Center = new Point(circleParms[0], circleParms[1], circleParms[2]);
                                        // full circle
                                        if (Math.Round(endParam - startParam, 4) == 2 * Math.Round(Math.PI, 4))
                                        {
                                           
                                           
                                            switch (plane)
                                            {
                                                case Plane_e.Unknown:
                                                    break;
                                                case Plane_e.XY:
                                                    {
                                                        Center.ZeroAxis(CoordinateToZero_e.Z);
                                                        
                                                    }
                                                    break;
                                                case Plane_e.YZ:
                                                    {
                                                        Center.ZeroAxis(CoordinateToZero_e.X);
                                                      
                                                    }
                                                    break;
                                                case Plane_e.ZX:
                                                    {
                                                        Center.ZeroAxis(CoordinateToZero_e.Y);
                                                        
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                            var endPoint = new Point(swCurveParams.EndPoint);
                                            Center = Center.TransformPointToNewCoordinateSystem(mathT, swMathUtility);                
                                            endPoint = endPoint.TransformPointToNewCoordinateSystem(mathT, swMathUtility);
                                            var Circle = new Circle(Center, radius);
                                            Shapes.Add(Circle);
                                        }

                                        // arc
                                        else
                                        {

                                           
                                           
                                            var startPoint = new Point(swCurveParams.StartPoint);

                                            System.Windows.Vector v = new System.Windows.Vector(startPoint.X - Center.X, startPoint.Y - Center.Y);
                                            System.Windows.Vector u = new System.Windows.Vector(MidPoint.X - Center.X, MidPoint.Y - Center.Y);
                                            double crossret = System.Windows.Vector.CrossProduct(u, v);
                                            var endPoint = new Point(swCurveParams.EndPoint);
                                            Direction dir = Direction.Unknown;


                                            if (swCurveParams.Sense)
                                            {
                                               
                                                dir = Direction.Cloackwise;
                                                if (loopInner)
                                                    if (swEdge.IEdgeInFaceSense2(swFace))
                                                    {
                                                        dir = Direction.CounterCloackwise;
                                                    }
                                                else
                                                    {
                                                        dir = Direction.Cloackwise;
                                                    }

                                            }
                                            else
                                            {
                                                 
                                                dir = Direction.CounterCloackwise;
                                                if (loopInner)
                                                    if (swEdge.IEdgeInFaceSense2(swFace))
                                                    {
                                                        dir = Direction.Cloackwise;
                                                    }
                                                    else
                                                    {
                                                        dir = Direction.CounterCloackwise;
                                                    }

                                            }
                                            switch (plane)
                                            {
                                                case Plane_e.Unknown:
                                                    break;
                                                case Plane_e.XY:
                                                    {
                                                        startPoint.ZeroAxis(CoordinateToZero_e.Z);
                                                        endPoint.ZeroAxis(CoordinateToZero_e.Z);
                                                        Center.ZeroAxis(CoordinateToZero_e.Z);
                                                        MidPoint.ZeroAxis(CoordinateToZero_e.Z);
                                                    }
                                                    break;
                                                case Plane_e.YZ:
                                                    {
                                                        startPoint.ZeroAxis(CoordinateToZero_e.X);
                                                        endPoint.ZeroAxis(CoordinateToZero_e.X);
                                                        Center.ZeroAxis(CoordinateToZero_e.X);
                                                        MidPoint.ZeroAxis(CoordinateToZero_e.X);
                                                    }
                                                    break;
                                                case Plane_e.ZX:
                                                    {
                                                        startPoint.ZeroAxis(CoordinateToZero_e.Y);
                                                        endPoint.ZeroAxis(CoordinateToZero_e.Y);
                                                        Center.ZeroAxis(CoordinateToZero_e.Y);
                                                        MidPoint.ZeroAxis(CoordinateToZero_e.Y);

                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }

                                            Center = Center.TransformPointToNewCoordinateSystem(mathT, swMathUtility);
                                            startPoint = startPoint.TransformPointToNewCoordinateSystem(mathT, swMathUtility);
                                            endPoint = endPoint.TransformPointToNewCoordinateSystem(mathT, swMathUtility);
                                            
                                            var Circle = new Circle(startPoint, endPoint, Center, dir, radius, Length);
                                            Circle.MidPoint = MidPoint;
                                            Shapes.Add(Circle);

                                        }


                                    }
                                    break;
                                case swCurveTypes_e.ELLIPSE_TYPE:
                                    {
                                        double[] EllipseParms = swCurve.GetEllipseParams();

                                        var Center = new Point(EllipseParms[0], EllipseParms[1], EllipseParms[2]);
                                        double MajorR = EllipseParms[3];
                                        double MinorR = EllipseParms[7];
                                        var Major = new Vector(EllipseParms[4], EllipseParms[5], EllipseParms[6]);
                                        var Minor = new Vector(EllipseParms[8], EllipseParms[9], EllipseParms[10]);
                                    }
                                    break;
                                case swCurveTypes_e.INTERSECTION_TYPE:
                                    break;
                                case swCurveTypes_e.BCURVE_TYPE:
                                    break;
                                case swCurveTypes_e.SPCURVE_TYPE:
                                    break;
                                case swCurveTypes_e.CONSTPARAM_TYPE:
                                    break;
                                case swCurveTypes_e.TRIMMED_TYPE:
                                    break;
                                default:
                                    break;
                            }

                           
                        }


                        Loop = Loop.GetNext();
                    }

                     

                        return Shapes.ToArray();
                    }


                return null;
               
            });

        }    
        private static bool areSOLIDWORKSVectorsCollinear(MathVector V, MathVector U)
        {
            MathVector ret = V.Cross(U) as MathVector;
            double[] d = ret.ArrayData;
            if (Math.Round(d[0], 6) == 0 && Math.Round(d[1], 6) == 0 && Math.Round(d[2], 6) == 0)
                return true;
            else
                return false;
        }
        private static bool areSOLIDWORKSVectorsPerpendicular(MathVector V, MathVector U)
        {
            double ret = V.Dot(U);

            if (Math.Round(ret, 6) == 0)
                return true;
            else
                return false;
        }
       



        #region extension methods
        public static bool DrawShapes(this Canvas Canvas, IShape[] Shapes, POCOS.Component Component = null)
        {
            // get bounding box
            double minX, minY, MinZ;

            List<Point> points = new List<Point>();
            foreach (IShape shape in Shapes)
            {
                points.AddRange(shape.Box());
            }

            minX = points.Min(point => point.X);
            minY = points.Min(point => point.Y);
            MinZ = points.Min(point => point.Z);


            Canvas.Children.Clear();
 
            var geometry = new GeometryGroup();
            foreach (var shape in Shapes)
            {
                switch (shape.GetType())
                {
                    case TwoDeeNest_e.ShapeType_e.Unknown:
                        break;
                    case TwoDeeNest_e.ShapeType_e.Circle:
                        {

                            var c = shape as Circle;
                            if (!c.IsTranslated)
                            {
                                c.Translate(-minX, -minY, -MinZ);
                                c.IsTranslated = true;

                                if (c.IsComplete())
                                {
                                    Debug.Print(c.Center.X.ToString());
                                    Debug.Print(c.Center.Y.ToString());
                                }
                            }
                            if (c.IsComplete() == false)
                            {//Arc
                                var g = new StreamGeometry();

                                using (var gc = g.Open())
                                {
                                    gc.BeginFigure(
                                        startPoint: new System.Windows.Point(c.StartPoint.X, c.StartPoint.Y),
                                        isFilled: true,
                                        isClosed: false);
                                    SweepDirection sweep = SweepDirection.Clockwise;
                                    switch (c.CircleDirection)
                                    {
                                        case Direction.Unknown:
                                            break;
                                        case Direction.Cloackwise:
                                            sweep = SweepDirection.Clockwise;
                                            break;
                                        case Direction.CounterCloackwise:
                                            sweep = SweepDirection.Counterclockwise;
                                            break;
                                        default:
                                            break;
                                    }

                                    if (c.Radius == 7.5)
                                        Debug.Print("ARC");
                                     
                                    bool isARCLarge = c.getLength()  >  c.Radius * Math.PI  ? true : false; 

                                    gc.ArcTo(
                                        point: new System.Windows.Point(c.EndPoint.X, c.EndPoint.Y),
                                        size: new System.Windows.Size(c.Radius, c.Radius),
                                        rotationAngle: 0d,
                                        isLargeArc: isARCLarge,
                                    sweepDirection: sweep,
                                        isStroked: true,
                                        isSmoothJoin: true);
                                }

                                var path = new WPFShapes.Path
                                {
                                    Stroke = Settings.DrawingColor,
                                    StrokeThickness = 0.5,
                                    Data = g
                                };

                                  Canvas.Children.Add(path);
                                
                            }
                            // circle
                            else
                            {

                                var WPFcircle = new WPFShapes.Ellipse();


                                WPFcircle.StrokeThickness = 0.5;
                                WPFcircle.Stroke = Settings.DrawingColor;

                                // Set the width and height of the Ellipse.
                                WPFcircle.Width = c.Radius * 2;
                                WPFcircle.Height = c.Radius * 2;

                                Canvas.SetLeft(WPFcircle, c.Center.X - c.Radius  );
                                Canvas.SetTop(WPFcircle, c.Center.Y  - c.Radius );
                                // Add the Ellipse to the StackPanel.
                                Canvas.Children.Add(WPFcircle);
                                
                            }
                        }
                        break;
                    case TwoDeeNest_e.ShapeType_e.Ellipse:
                        break;
                    case TwoDeeNest_e.ShapeType_e.Line:
                        {
                            var l = shape as Line;


                            var WPFLine = new WPFShapes.Line();
                            if (!l.IsTranslated)
                            {
                                l.Translate(-minX, -minY, -MinZ);
                                l.IsTranslated = true;
                            }
                            WPFLine.StrokeThickness = 0.5;
                            WPFLine.Stroke = Settings.DrawingColor;
                            WPFLine.X1 = l.StartPoint.X;
                            WPFLine.X2 = l.EndPoint.X;
                            WPFLine.Y1 = l.StartPoint.Y;
                            WPFLine.Y2 = l.EndPoint.Y;

                        Canvas.Children.Add(WPFLine);
                         

                        }
                        break;
                    default:
                        break;
                }

            }

          
            
           return true;
       }
        public static bool ZoomToFit(this Canvas Canvas, POCOS.Component Comp = null)
        {
            if (Comp == null)
                return false;

            var Compwidth = Comp.GetSize()[0];
            var Compheight = Comp.GetSize()[1];
            var Canvwidth = Canvas.ActualWidth;
            var Canvheight = Canvas.ActualHeight;

            double scaleX = Canvwidth / Compwidth;
            double scaleY = Canvheight / Compheight;
            double scale = 1;
            if (Canvwidth < Compwidth || Canvheight < Compheight)
                scale = new double[] { scaleX, scaleY }.Min() * 0.9;
            double offsetx = Canvwidth * 0.5 - Compwidth * 0.5;
            double offsety = Canvheight * 0.5 - Compheight * 0.5;
            TransformGroup t = new TransformGroup();
            var Translate = new TranslateTransform(offsetx, offsety);
            var Rotate = new ScaleTransform(scale, scale, Canvwidth * 0.5, Canvheight * 0.5);
            t.Children.Add(Translate);
            t.Children.Add(Rotate);
            foreach (WPFShapes.Shape child in Canvas.Children)
            {
                child.StrokeThickness = 1 / scale;
                child.RenderTransform = t;
            }
       
            Canvas.UpdateLayout();
            return true; 
        }
       
        #endregion

    }
}
