﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using SolidWorks.Extended;
using TwoDeeNest.Controls; 
using POCOS = TwoDeeNest.POCOS;
using System.Diagnostics;

namespace TwoDeeNest
{
   public partial class NewPlan : Form
    {
        
       
        public NewPlan(SldWorks _swApp)
        {
            InitializeComponent();
            
            this.elementHost1.Child = new Controls.MainView(_swApp);
            
        }

        private void NewPlan_Load(object sender, EventArgs e)
        {

        }


        //        private void NewPlan_Load(object sender, EventArgs e)
        //        {
        //        }
        //        private async void LoadAssemblyComponents()
        //        {
        //            swModel = swApp.ActiveDoc as ModelDoc2;
        //            swMathUtility = swApp.GetMathUtility() as MathUtility;
        //            if (swModel.GetType() == (int)swDocumentTypes_e.swDocASSEMBLY)
        //            {
        //                Components.AllowEdit = true;
        //                Components.AllowNew = true;
        //                Components.AllowRemove = true;
        //                var ComponentsList = new List<POCOS.Component>();
        //                AssemblyDoc swAssembly = swModel as AssemblyDoc;
        //                Component2[] swComponents = swAssembly.GetComponents(false, true,true);

        //                foreach (var Component in swComponents)
        //                {
        //                    if (ComponentsList.Exists((x) => x.FileName == Component.GetPathName()))
        //                    {
        //                        POCOS.Component component = ComponentsList.Find((x) => x.FileName == Component.GetPathName());
        //                        component.Count++;
        //                    }
        //                    else
        //                    {
        //                        ModelDoc2 ModelDoc = default(ModelDoc2);
        //                        try
        //                        {


        //                            //Status.Text = "Processing " + Component.GetPathName() + " ...";

        //                            ModelDoc = await swApp.OpenPartAsync(Component.GetPathName());

        //                            if (ModelDoc == null)
        //                            {
        //                                //swStatus.Text = "Unable to open " + Component.GetPathName() + " ...";
        //                                continue;
        //                            }

        //                            swStatus.Text = "Getting geometry of " + Component.GetPathName() + " ...";
        //                            var Feature = await Helper.GetFeatureFromSheetMetal(ModelDoc, swApp);
        //                            if (Feature == null)
        //                            {
        //                                swStatus.Text = "Skipping " + Component.GetPathName() + " ...";
        //                                await swApp.CloseDocAsync(ModelDoc);
        //                                continue;
        //                            }

        //                            Debug.Print(Feature.Name);                              
        //                            swStatus.Text = "Processing geometry of " + Component.GetPathName() + " ...";
        //                            var Face = await Helper.GetLargestFace2(Feature);
        //                            if (Face == null)
        //                            {
        //                                swStatus.Text = "Skipping " + Component.GetPathName() + " ...";
        //                                await swApp.CloseDocAsync(ModelDoc);
        //                                continue;
        //                            }

        //                            swStatus.Text = "Interpolating geometry of " + Component.GetPathName() + " ...";
        //                            var Points = await Helper.GetPointsFromFace(Face, 300, swMathUtility);
        //                            swStatus.Text = "Getting material information from " + Component.GetPathName() + " ...";
        //                            var Material = await Helper.GetMaterial(ModelDoc);
        //                            swStatus.Text = "Getting thickness of " + Component.GetPathName() + " ...";
        //                            var Thickness = await Helper.GetThickness(ModelDoc);
        //                            swStatus.Text = "Creating TwoDeeNest component...";                           
        //                            var NewComponent = new POCOS.Component(Points, Component.GetPathName(), Material, Thickness);

        //                            await swApp.CloseDocAsync(ModelDoc,Feature);
        //                            Components.Add(NewComponent);
        //                            ComponentsGrid.DataSource = Components;
        //                            ComponentsGrid.Refresh();

        //                        }
        //                        catch (Exception ee)
        //                        {
        //                            // treat logs
        //                            swStatus.Text = "An error occured. Please see logs.";
        //                            Debug.Print(ee.Message);
        //                            await swApp.CloseDocAsync(ModelDoc);
        //                            continue;
        //                        }
        //                    }
        //                }
        //                swProgressBar.Style = ProgressBarStyle.Continuous;
        //                swProgressBar.MarqueeAnimationSpeed = 0;
        //                swStatus.Text = "Ready!";


        //            }


        //        }
        //        private void NewPlan_VisibleChanged(object sender, EventArgs e)
        //        {
        //            var form = sender as Form;
        //            if (form.Visible)
        //            {
        //                LoadAssemblyComponents();
        //                ComponentsGrid.DataSource = Components;
        //            }

        //        }
        //        private void ComponentsGrid_SelectionChanged(object sender, EventArgs e)
        //        {
        //            DataGridView grid = sender as DataGridView;
        //            var selectedrows = grid.SelectedRows;
        //            if (selectedrows != null)
        //            {
        //                if (selectedrows.Count > 0)
        //                { 
        //                DataGridViewRow row = selectedrows[0];
        //                if (!row.IsNewRow)
        //                {
        //                    var SelectedCompnent = row.DataBoundItem as POCOS.Component;

        //                        var b = SelectedCompnent.GetBitmap();
        //                        pictureBox1.Image = b;
        //                        pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;


        //                }
        //                }
        //            }
        //        }

        //        private void button1_Click(object sender, EventArgs e)
        //        {

        //        }
    }
}