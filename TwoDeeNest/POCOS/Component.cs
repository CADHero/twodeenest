﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoDeeNest.Geometry;
using TwoDeeNest;
using System.ComponentModel;
using TwoDeeNest.TwoDeeNest_e;
using Drawing = System.Drawing;
using System.Drawing;
using System.Diagnostics;
using Geometry = TwoDeeNest.Geometry;

namespace TwoDeeNest.POCOS
{
    public class Component
    {
        
        public bool Exclude { get; set; }
        public string File { get; private set; }
        public int Count { get; set; }
        public string Material { get; private set; }
        public double Thickness { get; private set; }
        [DisplayName("Filename")]
        public string FileName { get; private set; }
        public Rotation_e[] Rotation { get; set; } 
        private Geometry.Point[] Points { get; set; }
        private Geometry.IShape[] Shapes { get; set; }
        private string ID { get; set; }
       
        public Component()
        {
        }

        public Component(Geometry.IShape[] shapes, string fileName, string material, double thickness)
        {
            Material = material;
            Thickness = thickness;
        
            Shapes=  shapes;
            FileName = fileName;
            File = System.IO.Path.GetFileNameWithoutExtension(fileName);
          
        }


        public IShape[] GetShapes()
        {
            return Shapes;
        }

        /// <summary>
        /// return width and height
        /// </summary>
        /// <returns></returns>
        public double[] GetSize()
        {
            double minY, minX, minZ, maxY, maxX, maxZ;
            List<Geometry.Point> points = new List<Geometry.Point>();
            foreach (IShape shape in Shapes)
            {
                points.AddRange(shape.Box());
            }

            minX = points.Min(point => point.X);
            minY = points.Min(point => point.Y);
            minZ = points.Min(point => point.Z);
            maxX = points.Max(point => point.X);
            maxY = points.Max(point => point.Y);
            maxZ = points.Max(point => point.Z);

            return new double[] { Math.Abs(maxX - minX), Math.Abs(maxY - minY), Math.Abs(maxZ - minZ) };

        }

    }
}
