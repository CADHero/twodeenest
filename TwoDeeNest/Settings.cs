﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TwoDeeNest
{
    public static class Settings
    {

        public static SolidColorBrush DrawingColor { get; set; }
     

        public static bool Init()
        {
            DrawingColor = Brushes.Blue;
            return true; 
        }
    }
}
