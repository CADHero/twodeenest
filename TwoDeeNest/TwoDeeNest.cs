﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using SolidWorks.Interop.swpublished;
using System.Runtime.InteropServices;

using Microsoft.Win32;
using System.Diagnostics;

namespace TwoDeeNest
{
    //[ProgId("TwoDeeNest.Application")]
    //[Guid("7579411E-045D-4849-90D8-A2F51ACFC70D")]
    //[ComVisible(true)]
    public class TwoDeeNest : SwAddin
    {
        int menuId;
        int CreatePlanMenuId;
        SldWorks swApp;
        int SessionCookie;
        #region SolidWorks connection      
        public bool ConnectToSW(object ThisSW, int Cookie)
        {
            swApp = ThisSW as SldWorks;
            swApp.SetAddinCallbackInfo(0, this, Cookie);
            SessionCookie = Cookie;
            addMenu();
            // this where we build the UI
            return true;

        }

        public void addMenu()


        {
            int ShowPlaces = (int)swDocumentTypes_e.swDocASSEMBLY;
            Debug.Print(swApp.RemoveMenu((int)swDocumentTypes_e.swDocASSEMBLY, "TwoDeeNest", "").ToString());
            Debug.Print(swApp.RemoveMenu((int)swDocumentTypes_e.swDocPART, "TwoDeeNest", "").ToString());
            menuId = swApp.AddMenu(ShowPlaces, "TwoDeeNest", 1);
            CreatePlanMenuId = swApp.AddMenuItem4(ShowPlaces, SessionCookie, "Create new plan@TwoDeeNest", 1, "NewPlan",null,"", null);
        }
        

        public void NewPlan()
        {

            var newPlan = new NewPlan(swApp);
            newPlan.Show();

        }

        public bool DisconnectFromSW()
        {
            // this is where we destroy the UI

            GC.Collect();
            swApp = null;

            return true;
        }
        #endregion

        #region com register-unregister functions
        [ComRegisterFunction]
        private static void RegisterAssembly(Type t)
        {
            string Path = String.Format(@"SOFTWARE\SolidWorks\AddIns\{0:b}", t);
            RegistryKey Key = Registry.LocalMachine.CreateSubKey(Path);
            // startup int
            Key.SetValue(null, 1);
            Key.SetValue("Title", "TwoDeeNest");
            Key.SetValue("Description", "Sheetmetal nesting addin by CADHERO Consulting");
        }
        [ComUnregisterFunction]
        private static void UnregisteryAssembly(Type t)
        {
            string Path = String.Format(@"SOFTWARE\SolidWorks\AddIns\{0:b}", t);
            Registry.LocalMachine.DeleteSubKey(Path);
        }

        #endregion
    }
}
