﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDeeNest
{
    namespace TwoDeeNest_e
    {
        public enum MouseHandlingMode
        {
            /// <summary>
            /// Not in any special mode.
            /// </summary>
            None,

            /// <summary>
            /// The user is left-dragging rectangles with the mouse.
            /// </summary>
            DraggingRectangles,

            /// <summary>
            /// The user is left-mouse-button-dragging to pan the viewport.
            /// </summary>
            Panning,

            /// <summary>
            /// The user is holding down shift and left-clicking or right-clicking to zoom in or out.
            /// </summary>
            Zooming,
        }
        public enum ShapeType_e
        {
            Unknown,
            Circle,
            Ellipse,
            Line

        }
        public enum RotationDir
        {
            Unknown,
            Cloackwise,
            Countercloackwise,
        }

        public enum Plane_e
        {
            Unknown,
            XY,
            YZ,
            ZX,

        }

        public enum CoordinateToZero_e
        {
            Unknown,
            X,
            Y,
            Z,
        }

        public enum Rotation_e
        {
            Ten,
            Twenty,
            Thirty,
            FourtyFive,
            Sixty,
            SeventyFive,
            Ninety,
            OneEighty,
        }
    }
}
